import React, { Component } from 'react';
import queryString from 'query-string'
import './App.css'
import spinner from './spinner.gif'
/*
*   This is the Inspect Component that allows the user to inspect the selected element
*   from the Home Component in greater detail
*/
export default class Inspect extends Component {
    constructor(props) {
        super(props);
        this.handleSearch = this.handleSearch.bind(this);
        this.state = { isLoaded: false, item: null, error: null};
    }
    // called after the component mounts
    componentDidMount(){
        // parsing the querystring to get the ID of the selected pic
        const pic = queryString.parse(this.props.location.search)
        const picID = pic.ID
        //fetch the specific pic based on the ID
        fetch("https://pixabay.com/api/?key=15190678-c67f7fc2dd5791a41ae5447aa&id="+ picID +"&image_type=photo").then
        (res => res.json()).then(
                (result) => {
                    // when the resulting JSON object is recieved, set loaded to true and item to the relevant object data
                    this.setState({
                        isLoaded: true,
                        item: result.hits[0]
                    });
                },
                // if there is an error with the fetch request set the following
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    // method to handle the search function
    handleSearch(){
        let search = this.state.inputValue;
        window.location.href = "/" + "?search=" + search;
    }

    // method to update the inputValue in state when the user types into the search bar
    updateInputValue(evt){
        this.setState({
            inputValue: evt.target.value
          });
    }

    render() {
        const {item, isLoaded, error} = this.state;
        // quick check if there has been any problems with the fetch request
        if(error){
            return (
            <div className="err">Sorry, error occurred: {error.message}</div>
            )
        }else if(!isLoaded){
            return(
                <div className="loading"><img src={spinner} alt="loading..." /></div>
            )
        }else{
            // upon successful fetch request, display the image that the user has selected
            return(
                <div className="Inspect container-fluid col-12 nopadding">
                    <div className="Home-header container col-12 nopadding">
                        <div className="row headerRow">
                            {/* the onchange event updates the inputValue in state */}
                            <div className="searchBar">
                                <input value={this.state.inputValue} onChange={evt => this.updateInputValue(evt)} type="text" className="search " aria-label="Large" aria-describedby="inputGroup-sizing-sm" placeholder="Search..."/>
                                <button data-testid="srch" className="btn search searchButton" onClick={ () => {this.handleSearch().bind(this)}}><i className="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div className="row">
                        {/* A large view of the image the user selected */}
                        <div className="col-md-12 col-lg-6 col-12 inspectDiv">
                            <img className="col-12" src={item.largeImageURL} />
                        </div>
                        {/* A table to show the information associated with that image */}
                        <table className="table tableSize table-hover col-md-12 col-lg-6 col-12">
                            <tbody>
                                <tr>
                                <th scope="row">Photographer </th>
                                <td className="tableUser">{item.user}</td>
                                </tr>
                                <tr>
                                <th scope="row">Tags</th>
                                <td>{item.tags}</td>
                                </tr>
                                <tr>
                                <th scope="row">Views</th>
                                <td >{item.views}</td>
                                </tr>
                                <tr>
                                <th scope="row">Downloads</th>
                                <td >{item.downloads}</td>
                                </tr>
                                <tr>
                                <th scope="row">Size</th>
                                <td >{item.imageSize}</td>
                                </tr>
                            </tbody>
                            </table>
                    </div>
                </div> 
            );
        }
    }
}