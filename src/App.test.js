import React from 'react';
import Home from './Home';
import Inspect from './Inspect'
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });
import { shallow, mount, render } from 'enzyme';

beforeAll(() => {
  global.fetch = jest.fn();
});
let wrapper;

// function for unit testing to simulate the data veing loaded after the API call in the Home component
function testLoadedHome(wrap) {
    wrap.setState({
      isLoaded: true,
      items: [
          { id: 1,
            largeImageUrl: 'aehbaefeu'
          }
        ]
  });
}

// function for unit testing to simulate an error occurring during the API call in the Home component
function testErrorHome(wrap){
    wrap.setState({
        error: "problem"
    });
}

// function for unit testing to simulate the data veing loaded after the API call in the Home component
function testLoadedInspect(wrap){
  wrap.setState({
      isLoaded: true,
      item: 
          { id: 1,
            largeImageUrl: 'aehbaefeu',
            user: 'a',
            tags: 'b',
            views: 'c',
            downloads: 'd'
          }
  });
}

// function for unit testing to simulate an error occurring during the API call in the Inspect component
function testErrorInspect(wrap){
  wrap.setState({
      error: "problem"
  });
}

it("must render a loading div before api call success in the Home component", () => {
  wrapper = shallow(<Home />, { disableLifecycleMethods: true });
  expect(wrapper.find("div.loading").exists()).toBeTruthy();
  expect(wrapper.state("isLoaded")).toBe(false);
  wrapper.unmount();
});

it("must render the images after the api call is successful in the Home component", () => {
  wrapper = shallow(<Home />, { disableLifecycleMethods: true });
  testLoadedHome(wrapper);
  expect(wrapper.state("isLoaded")).toBe(true);
  expect(wrapper.find("div.featureDiv").exists()).toBeTruthy();
  wrapper.unmount();
})

it("must render the div that informs the user that no images exist that are associate with their search", () => {
  wrapper = shallow(<Home />, { disableLifecycleMethods: true });
  wrapper.setState({
    items: undefined
  });
  expect(wrapper.find("div.notExist").exists());
  wrapper.unmount();
})

it("must unmount the component in order to display relevant images to the search", () => {
  wrapper = shallow(<Home />, { disableLifecycleMethods: true });
  testLoadedHome(wrapper);
  wrapper.setState({
    inputValue: "test"
  });
  wrapper.instance().handleSearch();
  expect(wrapper.unmount());
  wrapper.unmount();
})

it("must unmount the component and route to the Inspect component when an image is clicked in the Home component", () => {
  wrapper = shallow(<Home />, { disableLifecycleMethods: true });
  testLoadedHome(wrapper);
  wrapper.find("div.featureDiv").simulate("click");
  expect(wrapper.unmount());
  wrapper.unmount();
})

it("must render an error div if an error occurs in the API call in the Home component", () => {
  wrapper = shallow(<Home />, { disableLifecycleMethods: true });
  testErrorHome(wrapper);
  expect(wrapper.find("div.err").exists()).toBeTruthy();
  expect(wrapper.state("isLoaded")).toBe(false);
  wrapper.unmount();
})

it("must render a loading div before api call success in the Inspect component", () => {
  wrapper = shallow(<Inspect />, { disableLifecycleMethods: true });
  expect(wrapper.find("div.loading").exists()).toBeTruthy();
  expect(wrapper.state("isLoaded")).toBe(false);
  wrapper.unmount();
});

it("must render an error div if an error occurs in the API call in the Inspect component", () => {
  wrapper = shallow(<Inspect />, { disableLifecycleMethods: true });
  testErrorInspect(wrapper);
  expect(wrapper.find("div.err").exists()).toBeTruthy();
  expect(wrapper.state("isLoaded")).toBe(false);
  wrapper.unmount();
});

it("must render the image and the insformation associated with it after the api call is successful in the Inspect component", () => {
  wrapper = shallow(<Inspect />, { disableLifecycleMethods: true });
  testLoadedInspect(wrapper);
  expect(wrapper.state("isLoaded")).toBe(true);
  expect(wrapper.find("td.tableUser").exists()).toBeTruthy();
  wrapper.unmount();
})

