import React, { Component }from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import './App.css';
import Home from './Home';
import Inspect from './Inspect';

/*
* This is the main app component that handles the routes for displaying the other
* components
*/
export default class App extends Component {
  
  render(){
    return (
      <Router>
        <div className="App">
          <div className="App-Header">
          </div>
          {/* routing between components here */}
          <Switch>
              <Route exact path='/' component={Home} />
              <Route path='/inspect' component={Inspect} />
            </Switch>
        </div>
      </Router>
    );

  }
  
}
