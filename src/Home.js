import React, { Component } from 'react';
import queryString from 'query-string'
import './App.css'
import spinner from './spinner.gif'

/*
*   This is the Home component where the main content will be displayed
*/
export default class Home extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.state = { isLoaded: false, items: [], error: null, inputValue: ''};
    }

    // called after the component mounts
    componentDidMount(){
        // added a check to see if the user has searched for something 
        if(this.props.location !== undefined){
            let search = queryString.parse(this.props.location.search).search;
            // standard page defaults to a search for 'tech' related images
            if(search === undefined){
                search = 'tech';
            }
            // Making the search variable in case the user makes use of the search function
            fetch("https://pixabay.com/api/?key=15190678-c67f7fc2dd5791a41ae5447aa&q="+search+"&image_type=photo&pretty=true&per_page=200").then
            (res => res.json()).then(
                // when the resulting JSON object is recieved, set loaded to true and add the resulting data to the items array
                    (result) => {
                        console.log(result);
                        this.setState({
                            isLoaded: true,
                            items: result.hits
                        });
                    },
                    // if there is an error with the fetch request set the following
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                )
        }else{
            console.log("Location undefined");
        }
    }

     // method to handle the link to the next page
    handleClick(id){
        // direct the user to the Inspect component while passing the ID of the selected image as a query parameter
        window.location.href = "/inspect" + "?ID=" + id;
    }

    // method to handle the search function
    handleSearch(){
        let search = this.state.inputValue;
        window.location.href = "/" + "?search=" + search;
    }

    // method to update the inputValue in state when the user types into the search bar
    updateInputValue(evt){
        this.setState({
            inputValue: evt.target.value
          });
    }

    render() {
        const {items, isLoaded, error} = this.state;
        // quick check if there has been any problems with the fetch request
        if(error){
            return (
            <div className="err">Sorry, error occurred: {error.message}</div>
            )
        }else if(!isLoaded){
            return(
                <div className="loading"><img src={spinner} alt="loading..." /></div>
            )
            // added an error check for the occasion that a user man search for a string that does not match any images from the API
        }else if(items[0] === undefined){
            return(
                <div className="notExist">Sorry, no such images exist</div>
            )
        }else{
            // upon successful fetch request, display the images that were retrieved in the request
            return(
                <div className="Home container-fluid col-12 nopadding" >
                    <div className="Home-header container col-12 nopadding">
                        <div className="row headerRow">
                            {/* the onchange event updates the inputValue in state */}
                            <div className="searchBar">
                                <input value={this.state.inputValue} onChange={evt => this.updateInputValue(evt)} type="text" className="search " aria-label="Large" aria-describedby="inputGroup-sizing-sm" placeholder="Search..."/>
                                <button data-testid="srch" className="btn search searchButton" onClick={ () => {this.handleSearch().bind(this)}}><i className="fas fa-search"></i></button>
                            </div>
                        </div>         
                    </div>
                    <hr/>
                    <div className="container homeContainer col-12 col-lg-12">
                        <div className="d-flex flex-wrap  justify-content-center">
                            {/* displaying each of the images in the items array */}
                            {items.map(item => (
                                <div className="col-sm-4 col-lg-3 col-12 featureDiv" key={item.id}>
                                    <img data-testid="image" onClick={ () => {this.handleClick(item.id).bind(this)}} className="col-12 feature" src={item.largeImageURL} />
                            </div>
                            ))}
                        </div>
                    </div> 
                </div>
            );
        }
    }
}