# codeChallenge

Design Wizard Coding Exercise 

# To run the project:
    1 - clone the git repository
    2 - install the dependencies with npm install
    3 - run the app with npm start

A live version of the app can be found at https://reactcodeexercise.herokuapp.com/

This project pulls images and their related data from pixabay.com through their API. 
The images are displayed in a grid format and the user may select any of the images to view further details relating to said image.
There is also a search function so the user may view images relating to their desired input.
The project has been fully tested with unit tests in the App.test.js file. These unit tests can be run by running the command: npm test
(Note: All of the tests pass successfully, however there is a console error that appears due to navigation not being implemented in the console when window.location.href is called to navigate between components. This does not affect the functionality of the app itself.)